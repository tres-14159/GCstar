#!/usr/bin/perl

use strict;
use File::Copy;

my $baseDir = '../gcstar/lib/gcstar/GCLang/';

if ($#ARGV < 0)
{
    print "USAGE : $0 [-c|-i|-e] reference
    -c : for models translations
    -i : for import plugin translations
    -e : for export plugin translations
    Default is -c
    reference is the name of the language file (without GC and .pm).
    It should exist in English\n";

    exit;
}

chdir $baseDir;
my $type = 'GCModels';
if  ($#ARGV > 0)
{
    $type = 'GCImport' if $ARGV[0] eq '-i';
    $type = 'GCExport' if $ARGV[0] eq '-e';
    shift @ARGV;
}
my $translation = 'GC'.$ARGV[0].'.pm';
my $reference = "EN/$type/$translation";

print "Source is $reference\n";
if (! -f $reference)
{
    print "Reference not found\n";
    exit;
}

foreach my $lang(glob('*'))
{
    next if $lang =~ /(CVS|EN)/;
    my $new = "$lang/$type/$translation";
    next if -f $new;
    copy $reference, $new;
    open IN, $new;
    open OUT, ">$new.tmp";
    while (<IN>)
    {
        s/package GCLang::EN/package GCLang::$lang/;
        print OUT $_;
    }
    close IN;
    close OUT;
    move "$new.tmp", $new;
}
