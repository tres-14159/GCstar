#!/usr/bin/perl

use strict;
use Net::SMTP;
use Encode;
use MIME::Entity;

my $useNetSMTP = 1;

my $from = 'tian@gcstar.org';
my $to = 'gcstar-release@gna.org';
#my $to = 'tian@gcstar.org';
my $smtpServer = 'smtp.free.fr';

my $sendmail = '/usr/bin/sendmail';

my $version = $ARGV[0];
my $subject = "GCstar $version";

open CHANGELOG, "../gcstar/CHANGELOG";
binmode( CHANGELOG, ':utf8' );
my $changelog_en = '';
my $in = 0;
while (<CHANGELOG>)
{
    last if m/^\-\-/;
    $changelog_en .= $_ if $in;
    $in = 1 if m/^[0-9]/;
}
close CHANGELOG;

open CHANGELOG, "../gcstar/CHANGELOG.fr";
binmode( CHANGELOG, ':utf8' );
my $changelog_fr = '';
$in = 0;
while (<CHANGELOG>)
{
    last if m/^\-\-/;
    $changelog_fr .= $_ if $in;
    $in = 1 if m/^[0-9]/;
}
close CHANGELOG;

open MAIL, "mail.template";
binmode( MAIL, ':utf8' );
my $holdTerminator = $/;
undef $/;
my $mail = <MAIL>;
$mail =~ s/XXX/$version/g;
$mail =~ s/CHANGELOG.fr/$changelog_fr/;
$mail =~ s/CHANGELOG.en/$changelog_en/;

#$mail = "From: $from\r\nTo: $to\r\nSubject: $subject\r\n\r\n$mail";

if ($useNetSMTP)
{
 
    my $entity = MIME::Entity->build(
        Type => "text/plain",
        Charset => "UTF-8",
        Encoding => "quoted-printable",
        Data => $mail,
        From => Encode::encode(
            "MIME-Header", $from
        ),
        To => Encode::encode(
            "MIME-Header", $to
        ),
        Subject => Encode::encode(
            "MIME-Header", $subject
        ),
    );
 
    my $smtp = Net::SMTP->new($smtpServer);
    $smtp->mail($from);
    $smtp->recipient($to);
    $smtp->data;
    #$smtp->datasend($mail);
    my $msg = $entity->stringify;
    while ( $msg =~ m/([^\r\n]*)(\r\n|\n\r|\r|\n)?/g ) {
        my $line = ( defined($1) ? $1 : "" ) . "\r\n";
        $smtp->datasend( $line );
    }
    $smtp->dataend;
    $smtp->quit;    
}
else
{
    $! = '';
    if (open(MAILER, "| $sendmail -t -ba"))
    {
        open PIPO, ">pipo.txt";
        print PIPO "MAIL : ",$mail,"\n";
        print MAILER "$mail\r\n.\r\n";
        close MAILER;
    }
    else
    {
        print "Fatal error sending mail\n";
        exit 1;
    }
}
exit 0;